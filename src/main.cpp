#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "../include/L1cache.h"
#include "../include/L2cache.h"
#include "../include/Victimcache.h"
#include "../include/debug_utilities.h"

#define CHAR_SIZE 100
#define ADDR_SIZE 9
#define VC_ASSOCIATIVITY 16
#define VC_IDX_SIZE 1
#define VC_IDX 0

using namespace std;

int main(int argc, char * argv [])
{
  /* Parse argruments */
  uint16_t l1_cache_size_kb, block_size_bytes, l1_associativity, optimization;

  for (size_t i = 0; i < argc; i++)
  {
    /* Cache size */
    if (strcmp(argv[i], "-t") == 0)
    {
      l1_cache_size_kb = atoi(argv[i+1]);
    }
    /* Line size in bytes */
    else if (strcmp(argv[i], "-l") == 0)
    {
      block_size_bytes = atoi(argv[i+1]);
    }
    /* Associativity */
    else if (strcmp(argv[i], "-a") == 0)
    {
      l1_associativity = atoi(argv[i+1]);
    }
    /* Optimization */
    else if (strcmp(argv[i], "-opt") == 0)
    {
    	if (strcmp(argv[i+1], "vc") == 0)
    	{
    		optimization = 0;
    	}
    	else if (strcmp(argv[i+1], "l2") == 0)
    	{
    		optimization = 1;
    	}
    	else
    	{
    		cout << "Optimization not found";
    	}
    }
  }

  /* New operation results */
  struct operation_result l1_result;
  struct operation_result l2_result;
	struct operation_result vc_result;

	/* New statistics results */
	struct stat_result l1_stat_result = {0,0,0,0,0};
	struct stat_result l2_stat_result = {0,0,0,0,0};
	struct stat_result vc_stat_result = {0,0,0,0,0};

	/* New caches */
  entry **l1_cache_blocks;
	entry **l2_cache_blocks;
	entry **vc_cache_blocks;

  /* Get field (tag, idx, offset ) for L1 cache*/
  int l1_tag_size;
  int l1_idx_size;
  int l1_offset_size;

  field_size_get(l1_cache_size_kb, l1_associativity, block_size_bytes, &l1_tag_size, &l1_idx_size, &l1_offset_size);

  l1_cache_blocks = cache_matrix(l1_associativity, l1_idx_size);

  /* Choose optimization */
  uint16_t l2_associativity = 2*l1_associativity;
  uint16_t l2_cache_size_kb = 4*l1_cache_size_kb;

  int l2_tag_size;
  int l2_idx_size;
  int l2_offset_size;

  switch (optimization)
  {
  	case VC:
  		vc_cache_blocks = cache_matrix(VC_ASSOCIATIVITY, VC_IDX_SIZE);
  		break;
  	case L2:
  		/* Get field size (tag, idx, offset ) for L2 cache*/
		  field_size_get(l2_cache_size_kb, l2_associativity, block_size_bytes, &l2_tag_size, &l2_idx_size, &l2_offset_size);

		  /* New L2 cache */
  		l2_cache_blocks = cache_matrix(l2_associativity, l2_idx_size);
  		break;
  }

  /* Get trace's lines and start your simulation */
  char address_hex[ADDR_SIZE];
  char data_in[CHAR_SIZE];

  bool debug = false;
  int loadstore;
  long address;

  int l1_idx, l1_tag;
  int l2_idx, l2_tag;

  while (fgets(data_in, 100, stdin) != NULL)
  {
    /* Parse line */
    if (data_in[0] == 35) // # is 35 in ASCII: no hash, no new entries
    {
      /* Get loadstore and address from entry */
      sscanf(data_in, "%*s %d %s", &loadstore, address_hex);

      /* Address from hex to decimal (char array to long) */
      stringstream ss;
      ss << hex << address_hex;
      ss >> address;
    }
    else
    {
      break;
    }

    /* Get tag and index from address for L1 cache */
    address_tag_idx_get(address, l1_tag_size, l1_idx_size, l1_offset_size, &l1_idx, &l1_tag);

    /* Choose optimization */
    switch (optimization)
    {
    	case VC:
		  	{
		  		/* Fill l1_vc_info*/
		  		struct l1_vc_entry_info l1_vc_info = {l1_tag, l1_idx, l1_associativity,VC_ASSOCIATIVITY};

		  		/* Run optimization */
		  		lru_replacement_policy_l1_vc(&l1_vc_info, loadstore,l1_cache_blocks[l1_idx], vc_cache_blocks[VC_IDX], &l1_result, &vc_result, debug);
		  	}
    		break;
    	case L2:
	    	{
	    		/* Get tag and index from address for L2 cache */
	    		address_tag_idx_get(address, l2_tag_size, l2_idx_size, l2_offset_size, &l2_idx, &l2_tag);

	    		/* Fill l1_l2_info*/
	    		struct l1_l2_entry_info l1_l2_info = {l1_tag, l1_associativity, l2_tag, l2_associativity };

	    		/* Run optimization */
	  			lru_replacement_policy_l1_l2(&l1_l2_info, loadstore, l1_cache_blocks[l1_idx], l2_cache_blocks[l2_idx], &l1_result, &l2_result, debug);
	  		}
    		break;
    }

    /* Get statistics */
    get_statistics(&l1_result, &l1_stat_result);
    get_statistics(&l2_result, &l2_stat_result);
    get_statistics(&vc_result, &vc_stat_result);
  }

  /* Print cache configuration */
  print_cache_config( l1_cache_size_kb, block_size_bytes, l1_associativity, optimization );

  /* Print Statistics */
  print_statistics( optimization, &l1_stat_result, &l2_stat_result, &vc_stat_result );

	return 0;
}
