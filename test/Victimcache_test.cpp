/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

using namespace std;

class VCcache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: Verifies miss in L1 and hit in VC
 * 1. Generate random config data
 * 2. Create entry info for block A
 * 3. Create L1 and VC caches
 * 4. Insert block A
 * 5. Fill L1 cache with some other blocks different to A
 * 6. Reference A block again
 * 7. Check for a miss on L1
 * 8. Check for a hit in VC
 */
TEST_F(VCcache,l1_miss_vc_hit){
  int status = OK;
  struct operation_result l1_result = {};
  struct operation_result vc_result = {};

  /* Generate random config data */
  int l1_idx = rand()%1024;
  int l1_tag = rand()%4096;
  int l1_associativity = 1 << (rand()%4);
  int vc_associativity = 16;
  int vc_idx = 0;

  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity l1: %d\n associativity vc: %d\n",
          l1_idx,
          l1_tag,
          l1_associativity,
          vc_associativity);
  }

  /* Create entry info for block A */
  int A_tag = l1_tag;
  struct l1_vc_entry_info A_info = {A_tag, l1_idx, l1_associativity, vc_associativity};

  /* Create L1 and VC caches */
  struct entry vc_cache_blocks[vc_associativity] = {};
  struct entry l1_cache_blocks[l1_associativity] = {};

  /* Insert block A (load) */
  status = lru_replacement_policy_l1_vc(&A_info, LOAD,l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
  EXPECT_EQ(status, OK);

  /* Fill L1 cache with some other blocks different to A */
  struct l1_vc_entry_info B_info = {0, l1_idx, l1_associativity, vc_associativity};
  for (int i = 0; i < l1_associativity; ++i)
  {
    B_info.l1_tag = rand()%1024;
    while(B_info.l1_tag == A_tag)
    {
      B_info.l1_tag = rand()%1024;
    }
    status = lru_replacement_policy_l1_vc(&B_info, LOAD, l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
    EXPECT_EQ(status, OK);
  }

  /* Reference A block again */
  status = lru_replacement_policy_l1_vc(&A_info, LOAD, l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
  EXPECT_EQ(status, OK);

  /* Check for a miss on L1 */
  DEBUG(debug_on, l1_miss_test);
  EXPECT_EQ(l1_result.miss_hit, MISS_LOAD);

  /* Check for a hit in VC */
  DEBUG(debug_on, vc_hit_test);
  EXPECT_EQ(vc_result.miss_hit, HIT_LOAD);
}

/*
 * TEST2: Verifies miss in L1 and miss in VC
 * 1. Generate random config data
 * 2. Create entry info for block A
 * 3. Create L1 and VC caches
 * 4. Fill L1 cache with some other blocks different to A
 * 5. Reference block A
 * 7. Check for a miss on L1
 * 8. Check for a miss in VC
 */
TEST_F(VCcache,l1_miss_vc_miss){
  int status = OK;
  struct operation_result l1_result = {};
  struct operation_result vc_result = {};

  /* Generate random config data */
  int l1_idx = rand()%1024;
  int l1_tag = rand()%4096;
  int l1_associativity = 1 << (rand()%4);
  int vc_associativity = 16;
  int vc_idx = 0;

  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity l1: %d\n associativity vc: %d\n",
          l1_idx,
          l1_tag,
          l1_associativity,
          vc_associativity);
  }

  /* Create entry info for block A */
  int A_tag = l1_tag;
  struct l1_vc_entry_info A_info = {A_tag, l1_idx, l1_associativity, vc_associativity};

  /* Create L1 and VC caches */
  struct entry vc_cache_blocks[vc_associativity] = {};
  struct entry l1_cache_blocks[l1_associativity] = {};

  /* Fill L1 cache with some other blocks different to A */
  struct l1_vc_entry_info B_info = {0, l1_idx, l1_associativity, vc_associativity};
  for (int i = 0; i < l1_associativity; ++i)
  {
    B_info.l1_tag = rand()%1024;
    while(B_info.l1_tag == A_tag)
    {
      B_info.l1_tag = rand()%1024;
    }
    status = lru_replacement_policy_l1_vc(&B_info, LOAD, l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
    EXPECT_EQ(status, OK);
  }

  /* Reference block A */
  status = lru_replacement_policy_l1_vc(&A_info, LOAD, l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
  EXPECT_EQ(status, OK);

  /* Check for a miss on L1 */
  DEBUG(debug_on, l1_miss_test);
  EXPECT_EQ(l1_result.miss_hit, MISS_LOAD);

  /* Check for a miss in VC */
  DEBUG(debug_on, vc_miss_test);
  EXPECT_EQ(vc_result.miss_hit, MISS_LOAD);
}

/*
 * TEST3: Verifies false hit in VC
 * 1. Generate random config data
 * 2. Create entry info for block A
 * 3. Create L1 and VC caches
 * 4. Insert block A
 * 5. Fill L1 cache with some other blocks different to A
 * 6. Reference A block again
 * 7. Check for a miss on L1
 * 8. Check for a hit in VC
 */
TEST_F(VCcache,l1_miss_vc_false_hit){
  int status = OK;
  struct operation_result l1_result = {};
  struct operation_result vc_result = {};

  /* Generate random config data */
  int l1_idx_A = rand()%1024;
  int l1_idx_B = rand()%1024;
  while(l1_idx_B == l1_idx_A)
  {
    l1_idx_B = rand()%1024;
  }
  int l1_tag = rand()%4096;
  int l1_associativity = 1 << (rand()%4);
  int vc_associativity = 16;
  int vc_idx = 0;

  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity l1: %d\n associativity vc: %d\n",
          l1_idx_A,
          l1_tag,
          l1_associativity,
          vc_associativity);
  }

  /* Create entry info for block A */
  int A_tag = l1_tag;
  struct l1_vc_entry_info A_info = {A_tag, l1_idx_A, l1_associativity, vc_associativity};

  /* Create entry info for block B (same tag, different idx) */
  int B_tag = l1_tag;
  struct l1_vc_entry_info B_info = {A_tag, l1_idx_B, l1_associativity, vc_associativity};

  /* Create L1 and VC caches */
  struct entry vc_cache_blocks[vc_associativity] = {};
  struct entry l1_cache_blocks[l1_associativity] = {};

  /* Insert block A (load) */
  status = lru_replacement_policy_l1_vc(&A_info, LOAD,l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
  EXPECT_EQ(status, OK);

  /* Fill L1 cache with some other blocks different to A */
  struct l1_vc_entry_info C_info = {0, l1_idx_A, l1_associativity, vc_associativity};
  for (int i = 0; i < l1_associativity; ++i)
  {
    C_info.l1_tag = rand()%1024;
    while(C_info.l1_tag == A_tag)
    {
      C_info.l1_tag = rand()%1024;
    }
    status = lru_replacement_policy_l1_vc(&C_info, LOAD, l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
    EXPECT_EQ(status, OK);
  }

  /* Reference B block again */
  status = lru_replacement_policy_l1_vc(&B_info, LOAD, l1_cache_blocks, vc_cache_blocks, &l1_result, &vc_result, bool(debug_on));
  EXPECT_EQ(status, OK);

  /* Check for a miss on L1 */
  DEBUG(debug_on, l1_miss_test);
  EXPECT_EQ(l1_result.miss_hit, MISS_LOAD);

  /* Check for a hit in VC */
  DEBUG(debug_on, vc_hit_test);
  EXPECT_EQ(vc_result.miss_hit, HIT_LOAD);

  /* Check tag between block A (the one inserted in VC) and block B (the one I'm referencing now) */
  DEBUG(debug_on, l1_vc_tag);
  EXPECT_EQ(A_tag, B_tag);

  /* Check idx between block A (the one inserted in VC) and block B (the one I'm referencing now) */
  DEBUG(debug_on, l1_vc_idx);
  EXPECT_EQ(l1_idx_A, l1_idx_B);
}

