/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bits/stdc++.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
#define HIT true
#define MISS false
#define STORE true
#define LOAD false
#define INITIAL_VALUE_empty false
#define LRU 0

using namespace std;

int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{
  /* Negative values banned */
  if(cachesize_kb <= 0 || associativity <= 0 || blocksize_bytes <= 0){
    return ERROR;
  }
  /* Are values power of 2? */
  if((ceil(log2(cachesize_kb*KB)) != floor(log2(cachesize_kb*KB))) ||
     (ceil(log2(associativity)) != floor(log2(associativity))) ||
     (ceil(log2(blocksize_bytes)) != floor(log2(blocksize_bytes)))){
       return ERROR;
     }
  /* Offset */
  *offset_size = log2(blocksize_bytes);
  /* Index */
  *idx_size = log2(cachesize_kb*KB)
            - log2(associativity)
            - (*offset_size);
  /* Tag */
  *tag_size = ADDRSIZE - (*idx_size) - (*offset_size);

  return OK;
}

void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{
  /* Tag */
  bitset<ADDRSIZE>tagSize(address);
  tagSize >>= idx_size + offset_size;
  *tag = (int)(tagSize.to_ulong());
  /* Index */
  bitset<ADDRSIZE>idxSize(address);
  idxSize <<= tag_size;
  idxSize >>= tag_size + offset_size;
  *idx = (int)(idxSize.to_ulong());
}

int l1_line_invalid_set(int tag,
                        int associativity,
                        entry* cache_blocks,
                        bool debug)
{
  for (int way = 0; way < associativity; ++way)
  {
    if ( cache_blocks[way].tag == tag )
    {
      cache_blocks[way].valid = false;
      return OK;
    }
  }
  return ERROR;
}

entry **cache_matrix (int associativity,
                      int idx_size)
{
  int ways = associativity;
  int sets = pow(2, idx_size);
  /*Cache Matrix sets*/
  entry **cache = new entry*[sets];
  /*Cache Matrix ways*/
  for (int way = 0; way < sets; way++) {
  cache[way] = new entry[ways];
  }
  /*Cache initialization for LRU replacement policy */
  /* way[0] = 0 | way[1] = 1 | ... | way[n] = n */
  for (int set = 0; set < sets; set++) {
    for (int way = 0; way < ways; way++) {
      cache[set][way].valid = false;
      cache[set][way].dirty = false;
      cache[set][way].tag = 0;
      cache[set][way].rp_value = way;
    }
  }
  return cache;
}
