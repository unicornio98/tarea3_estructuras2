/*
 * 	Cache simulation project
 * 	Class UCR IE-521
 * 	Semester: I-2019
*/
#ifndef DEBUG_UTILITIES
#define DEBUG_UTILITIES
#include "../include/L1cache.h"

#define CYN "\x1B[36m"
#define RESET "\x1B[0m"
#define YEL   "\x1B[33m"

/* 
 * ENUMERATIONS 
 */

/* Return Values */
enum optimization {
 VC,
 L2
};

/* Statistics result */
struct stat_result {
	int load_hits;
	int load_misses;
	int store_hits;
  int store_misses;
  int dirty_evictions;
};

/* MACROS */
#define DEBUG(y,x) if (y) printf(CYN "[INFO]:" RESET " %s\n",#x) 

/* FUNCTIONS */

/* Get enviroment var */
void get_env_var( const char* var_name,
                 int *var_value );

/* Print way info */
void print_way_info(int idx,
                    int associativity,
                    entry* cache_blocks); 

/* Print cache configuration */
void print_cache_config(uint16_t l1_cache_size_kb, uint16_t block_size_bytes, uint16_t l1_associativity, uint16_t optimization);

/* Get statistics */
void get_statistics( const operation_result* result , stat_result* stat_result);

/* Print statistics */
void print_statistics(uint16_t optimization, const stat_result* l1_result, const stat_result* l2_result, const stat_result* vc_result);
#endif
