/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "../include/debug_utilities.h"
#include "../include/L1cache.h"
#include "../include/L2cache.h"

#define KB 1024
#define ADDRSIZE 32
#define HIT true
#define MISS false
#define STORE true
#define LOAD false
#define INITIAL_VALUE_empty false

using namespace std;

int lru_replacement_policy_l1_l2(const l1_l2_entry_info *l1_l2_info,
																 bool loadstore,
																 entry* l1_cache_blocks,
																 entry* l2_cache_blocks,
																 operation_result* l1_result,
																 operation_result* l2_result,
																 bool debug)
{
	/* Negative values banned */
	if (l1_l2_info->l1_tag < 0 || l1_l2_info->l2_tag < 0) {
	 return PARAM;
	}
	/* Flag for hit or miss */
  bool l1_hit_or_miss = INITIAL_VALUE_empty;
	bool l2_hit_or_miss = INITIAL_VALUE_empty;
	/* Initial values */
	l2_result->dirty_eviction = false;
	l1_result->dirty_eviction = false;
	/* ------ CASE HIT L1------ */
	for (int way = 0; way < l1_l2_info->l1_associativity; way++) {
		/*--------------------------------------------------------------------
												L1 hit | L2 hit
		--------------------------------------------------------------------*/
		/* Same tag and valid */
		if ((l1_cache_blocks[way].tag == l1_l2_info->l1_tag) && l1_cache_blocks[way].valid) {
			/* Flag: hit in L1 */
			l1_hit_or_miss = HIT;
			/* Update rp_value L1 */
			for (int way_rp_value = 0; way_rp_value < l1_l2_info->l1_associativity; way_rp_value++) {
				if (l1_cache_blocks[way_rp_value].rp_value > l1_cache_blocks[way].rp_value) {
					l1_cache_blocks[way_rp_value].rp_value--;
				}
			}
			/* MRU L1 */
			l1_cache_blocks[way].rp_value = l1_l2_info->l1_associativity - 1;

			/* ------ then HIT L2 ------ */
			for (int wayl2 = 0; wayl2 < l1_l2_info->l2_associativity; wayl2++) {
				/* Same tag */
				if (l2_cache_blocks[wayl2].tag == l1_l2_info->l2_tag) {
					/* Flag: hit in L2 */
					l2_hit_or_miss = HIT;
					/* Update rp_value L2 */
					for (int way_rp_value = 0; way_rp_value < l1_l2_info->l2_associativity; way_rp_value++) {
						if (l2_cache_blocks[way_rp_value].rp_value > l2_cache_blocks[wayl2].rp_value) {
							l2_cache_blocks[way_rp_value].rp_value--;
						}
					}
					/* MRU L2 */
					l2_cache_blocks[wayl2].rp_value = l1_l2_info->l2_associativity - 1;
					/* Store */
					if (loadstore == STORE) {
						/* Dirty L2 */
						l2_cache_blocks[wayl2].dirty = true;
						/* HIT Store L2 */
						l2_result->miss_hit = HIT_STORE;
						/* HIT Store L1 */
						l1_result->miss_hit = HIT_STORE;
					} else { /* loadstore == LOAD */
						/* HIT Load L2 */
						l2_result->miss_hit = HIT_LOAD;
						/* HIT Load L1 */
						l1_result->miss_hit = HIT_LOAD;
					}
					/* break loop L2, hit found */
					break;
				}
			}
			/* break loop L1, hit found */
			break;
		}
	}
	/* ------ CASE MISS L1 ------ */
	if (l1_hit_or_miss == MISS) {
		/* Search HIT in L2 */
		for (int wayl2 = 0; wayl2 < l1_l2_info->l2_associativity; wayl2++) {
			/*--------------------------------------------------------------------
													L1 miss | L2 hit
			--------------------------------------------------------------------*/
			if (l2_cache_blocks[wayl2].tag == l1_l2_info->l2_tag && l2_cache_blocks[wayl2].valid) {
				/* Update rp_value L2 */
				for (int way_rp_value = 0; way_rp_value < l1_l2_info->l2_associativity; way_rp_value++) {
					if (l2_cache_blocks[way_rp_value].rp_value > l2_cache_blocks[wayl2].rp_value) {
						 l2_cache_blocks[way_rp_value].rp_value--;
					 }
			}
			/* MRU L2 */
			l2_cache_blocks[wayl2].rp_value = l1_l2_info->l2_associativity - 1;
			/* HIT L2 */
			l2_hit_or_miss = HIT;
			/* ------ Load or Store ------ */
			if (loadstore == STORE) {
				/* Dirty L2 */
				l2_cache_blocks[wayl2].dirty = true;
				/* HIT Store L2 */
				l2_result->miss_hit = HIT_STORE;
			} else { /* loadstore == LOAD */
				/* HIT Load L2 */
				l2_result->miss_hit = HIT_LOAD;
			}
			/* break loop L2, HIT found */
			break;
		}
	}
	/*--------------------------------------------------------------------
											L1 miss | L2 miss
	--------------------------------------------------------------------*/
	/* ------ CASE MISS L2 ------ */
	if (l2_hit_or_miss == MISS) {
		/* Replace in L1 */
		for (int wayl2 = 0; wayl2 < l1_l2_info->l2_associativity; wayl2++) {
			/* LRU */
			if (l2_cache_blocks[wayl2].rp_value == LRU){
				/* ------ Eviction ------ */
				/*Dirty*/
				if (l2_cache_blocks[wayl2].dirty == true ) {
					l2_result->dirty_eviction = true;
				}
				/*Address*/
				l2_result->evicted_address = l2_cache_blocks[wayl2].tag;
				//l1_line_invalid_set(l1_l2_info->l1_tag, l1_l2_info->l1_associativity, l1_cache_blocks, debug);
				/* ------ Load or Store ------ */
				if (loadstore == STORE) {
					l2_cache_blocks[wayl2].dirty = true;
					l2_result->miss_hit = MISS_STORE;
				} else { /* loadstore == LOAD */
					l2_cache_blocks[wayl2].dirty = false;
					l2_result->miss_hit = MISS_LOAD;
				}
				/* Update rp_value */
				for (int way_rp_value = 0; way_rp_value < l1_l2_info->l2_associativity; way_rp_value++) {
					if (l2_cache_blocks[way_rp_value].rp_value != LRU){
			 			l2_cache_blocks[way_rp_value].rp_value--;
					 }
				}
				/* MRU, update entry*/
				l2_cache_blocks[wayl2].rp_value = l1_l2_info->l2_associativity - 1;
				l2_cache_blocks[wayl2].valid = true;
				l2_cache_blocks[wayl2].tag = l1_l2_info->l2_tag;
				/* break loop L2, LRU found */
				break;
			}
		}
	}
/*--------------------------------------------------------------------*/
	/* Replace in L1 */
/*--------------------------------------------------------------------*/
	for (int way = 0; way < l1_l2_info->l1_associativity; way++) {
		/* LRU */
		if (l1_cache_blocks[way].rp_value == LRU){
			/* ------ Eviction ------ */
			/*Dirty*/
			//if (cache_blocks[way].dirty == true ) {
			//	result->dirty_eviction = true;
			//} else {
			l1_result->dirty_eviction = false;
			//}
			/*Address*/
			l1_result->evicted_address = l1_cache_blocks[way].tag;
			/* ------ Load or Store ------ */
			if (loadstore == STORE) {
				l1_result->miss_hit = MISS_STORE;
			} else { /* loadstore == LOAD */
				l1_result->miss_hit = MISS_LOAD;
			}
			/* Update rp_value */
			for (int way_rp_value = 0; way_rp_value < l1_l2_info->l1_associativity; way_rp_value++) {
				if (l1_cache_blocks[way_rp_value].rp_value != LRU){
		 			l1_cache_blocks[way_rp_value].rp_value--;
				 }
			}
			/* MRU, update entry*/
			l1_cache_blocks[way].rp_value = l1_l2_info->l1_associativity - 1;
			l1_cache_blocks[way].valid = true;
			l1_cache_blocks[way].tag = l1_l2_info->l1_tag;
			/* break loop, LRU found */
			break;
			}
		}
	}
	return OK;
}
