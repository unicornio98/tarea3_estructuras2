/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
#define HIT true
#define MISS false
#define INITIAL_VALUE_empty false
#define FIFO 0

using namespace std;

int fifo_replacement_policy_vc(const l1_vc_entry_info *l1_vc_info,
      	                      	 bool loadstore,
      	                      	 bool l1_valid,
      	                      	 int evicted_address,
          	                  	 entry* vc_cache_blocks,
              	              	 operation_result* vc_result,
                	            	 bool debug)
{
	/* Flag for hit or miss */
  bool vc_hit_or_miss = INITIAL_VALUE_empty;

	/* ------ CASE HIT ------ */
	for (int vc_way = 0; vc_way < l1_vc_info->vc_associativity; ++vc_way)
	{
		if (vc_cache_blocks[vc_way].tag == l1_vc_info->l1_tag && vc_cache_blocks[vc_way].valid)
		{
			vc_hit_or_miss = HIT;
			if (debug)
			{
				cout<< "VC hit\n";
			}

			/* Update result */
			if (loadstore == STORE) {
        vc_result->miss_hit = HIT_STORE;
      } else {
        vc_result->miss_hit = HIT_LOAD;
      }

      /* Switch data between L1 and VC */
      vc_cache_blocks[vc_way].tag = evicted_address;

      /* Update rp_value */
      for (int vc_way_rp_value = 0; vc_way_rp_value < l1_vc_info->vc_associativity; vc_way_rp_value++) {
        if (vc_cache_blocks[vc_way_rp_value].rp_value > vc_cache_blocks[vc_way].rp_value){
           vc_cache_blocks[vc_way_rp_value].rp_value--;
         }
      }
      /* MRU */
      vc_cache_blocks[vc_way].rp_value = l1_vc_info->vc_associativity - 1;
		}
		/* break loop, hit found */
		break;
	}
	/* ------ CASE MISS ------ */
	if (vc_hit_or_miss == MISS)
	{
		for (int vc_way = 0; vc_way < l1_vc_info->vc_associativity; vc_way++) {
      if (vc_cache_blocks[vc_way].rp_value == FIFO){ 
			/* Update result */
				if (loadstore == STORE) {
	        vc_result->miss_hit = MISS_STORE;

	      } else {
	        vc_result->miss_hit = MISS_LOAD;
	      }

	      if (l1_valid)
	      {
		      /* Update rp_value */
		      for (int vc_way_rp_value = 0; vc_way_rp_value < l1_vc_info->vc_associativity; vc_way_rp_value++) {
		         if (vc_cache_blocks[vc_way_rp_value].rp_value != FIFO){
		            vc_cache_blocks[vc_way_rp_value].rp_value--;
		         }
		      }

		      /* MRU, update entry*/
		      vc_cache_blocks[vc_way].rp_value = l1_vc_info->vc_associativity - 1;
		      vc_cache_blocks[vc_way].tag = evicted_address;
		      vc_cache_blocks[vc_way].valid = true;

		      /* Print VC*/
					if (debug)
					{
		      	cout << "VC miss: Inserté en VC\n";
						print_way_info(l1_vc_info->l1_idx, l1_vc_info->vc_associativity,vc_cache_blocks);
					}
	      }
	      else
	      {
	      	if (debug)
	      	{
	      		cout << "VC miss: l1 invalid\n";
	      	}
	      }

	      /* break loop, FIFO found */
	      break;
			}
		}
	}
  return OK;
}

int lru_replacement_policy_l1_vc(const l1_vc_entry_info *l1_vc_info,
      	                      	 bool loadstore,
        	                    	 entry* l1_cache_blocks,
          	                  	 entry* vc_cache_blocks,
            	                	 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	            	 bool debug)
{
   /* Negative values banned */
  if (l1_vc_info->l1_idx < 0 || l1_vc_info->l1_tag < 0) {
    return PARAM;
   }
  /* Flag for hit or miss */
  bool l1_hit_or_miss = INITIAL_VALUE_empty;

  /* ------ CASE HIT ------ */
  for (int way = 0; way < l1_vc_info->l1_associativity; way++) {
    /* Same tag and valid */
    if ((l1_cache_blocks[way].tag == l1_vc_info->l1_tag) && l1_cache_blocks[way].valid) {
    	if (debug)
    	{
	    	cout << "---------------CASE HIT---------------\n";
	    	cout << "tag: " << l1_vc_info->l1_tag <<", idx: "<< l1_vc_info->l1_idx << "\n";
    	}

      l1_hit_or_miss = HIT;
      /* ------ Load or Store ------ */
      if (loadstore == STORE) {
        l1_cache_blocks[way].dirty = true;
        l1_result->miss_hit = HIT_STORE;

      } else { /* loadstore == LOAD */
        l1_result->miss_hit = HIT_LOAD;
      }
      /* There isn't eviction */
      l1_result->dirty_eviction = false;

      /* Update rp_value */
      for (int way_rp_value = 0; way_rp_value < l1_vc_info->l1_associativity; way_rp_value++) {
        if (l1_cache_blocks[way_rp_value].rp_value > l1_cache_blocks[way].rp_value){
           l1_cache_blocks[way_rp_value].rp_value--;
         }
      }
      /* MRU */
      l1_cache_blocks[way].rp_value = l1_vc_info->l1_associativity - 1;

      /* break loop, hit found */
      break;
    }
  }
  /* ------ CASE MISS ------ */
  if (l1_hit_or_miss == MISS) {

    for (int way = 0; way < l1_vc_info->l1_associativity; way++) {
      /* LRU */
      if (l1_cache_blocks[way].rp_value == LRU){
      	if (debug)
      	{
      		cout << "---------------CASE MISS---------------\n";
      	}
        /* ------ Eviction ------ */
        /*Dirty*/
        if (l1_cache_blocks[way].dirty == true ) {
          l1_result->dirty_eviction = true;

        } else {
          l1_result->dirty_eviction = false;
        }
        /*Address*/
        l1_result->evicted_address = l1_cache_blocks[way].tag;

        if (debug)
        {
        	cout << "Tag: "<< l1_vc_info->l1_tag <<", idx: "<< l1_vc_info->l1_idx <<", Evicted address: " << l1_result->evicted_address << " valid: " << l1_cache_blocks[way].valid << "\n";
        }
        /* ------ Check on VC ------ */
        fifo_replacement_policy_vc(l1_vc_info, loadstore, l1_cache_blocks[way].valid , l1_cache_blocks[way].tag, vc_cache_blocks, vc_result, debug);

        /* ------ Load or Store ------ */
        if (loadstore == STORE) {
          l1_cache_blocks[way].dirty = true;
          l1_result->miss_hit = MISS_STORE;

        } else { /* loadstore == LOAD */
          l1_cache_blocks[way].dirty = false;
          l1_result->miss_hit = MISS_LOAD;
        }
        /* Update rp_value */
        for (int way_rp_value = 0; way_rp_value < l1_vc_info->l1_associativity; way_rp_value++) {
           if (l1_cache_blocks[way_rp_value].rp_value != LRU){
              l1_cache_blocks[way_rp_value].rp_value--;
           }
        }
        /* MRU, update entry*/
        l1_cache_blocks[way].rp_value = l1_vc_info->l1_associativity - 1;
        l1_cache_blocks[way].valid = true;
        l1_cache_blocks[way].tag = l1_vc_info->l1_tag;

        /* break loop, LRU found */
        break;
      }
    }
  }
  return OK;
}
