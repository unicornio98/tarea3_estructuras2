/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define LOAD 0
#define STORE 1
#define INVALID 0
using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: L1 hit L2 hit
 * 1. Choose a random entry info
 * 2. Fill a L1cache and L2cache entry
 * 3. Force a read/write hit
 * 4. Check hit in L1cache and L2cache
 */
TEST_F(L2cache,l1_hit_l2_hit_test){
	int status = OK;
	bool debug = false;
	struct l1_l2_entry_info l1_l2_info;
  struct operation_result l1_result = {};
  struct operation_result l2_result = {};

	/* 1. Choose a random entry info */
	int associativityL1 = 1 << (rand()%4);
	int associativityL2 = associativityL1 * 2;

	if (debug_on) {
	printf("Entry Info\n associativity l1: %d\n associativity l2: %d\n",
				associativityL1,
				associativityL2);
	}
	/* associativity */
	l1_l2_info.l1_associativity = associativityL1;
	l1_l2_info.l2_associativity = associativityL2;
	/* Set entries (0 read 1 write)*/
	struct entry l1_cache_blocks[2][l1_l2_info.l1_associativity];
	struct entry l2_cache_blocks[2][l1_l2_info.l2_associativity];
	/* 2. Fill a L1cache and L2cache entry */
	for (int i = 0 ; i < 2; i++){
		for (int wayl1 = 0; wayl1 < l1_l2_info.l1_associativity; wayl1++) {
			int rp_value_l1 = wayl1;
			int tag = wayl1;
			l1_cache_blocks[i][wayl1].valid = true;
			l1_cache_blocks[i][wayl1].tag = wayl1;
			l1_cache_blocks[i][wayl1].dirty = 0;
			l1_cache_blocks[i][wayl1].rp_value = rp_value_l1;
		}
		for (int wayl2 = 0; wayl2 < l1_l2_info.l2_associativity; wayl2++) {
			int rp_value_l2 = wayl2;
			int tag = wayl2;
			l2_cache_blocks[i][wayl2].valid = true;
			l2_cache_blocks[i][wayl2].tag = wayl2;
			l2_cache_blocks[i][wayl2].dirty = i;
			l2_cache_blocks[i][wayl2].rp_value = rp_value_l2;
		}
	}
	/* 3. Force a read/write hit */
	for (int loadstore = 0; loadstore < 2; loadstore++) {
		l1_l2_info.l1_tag = l1_l2_info.l1_associativity - 1;
		l1_l2_info.l2_tag = l1_l2_info.l2_associativity - 1;
		status = lru_replacement_policy_l1_l2(&l1_l2_info,
																	 				loadstore,
																	 				l1_cache_blocks[loadstore],
																	        l2_cache_blocks[loadstore],
																	        &l1_result,
																	 				&l2_result,
																					bool(debug_on));
		/* 4. Check hit in L2cache */
		DEBUG(debug_on, l1_hit_l2_hit_test);
		int hitl1;
		int hitl2;
		switch (loadstore) {
			case LOAD:
								hitl1 = HIT_LOAD;
								hitl2 = HIT_LOAD;
								break;
			case STORE:
								hitl1 = HIT_STORE;
								hitl2 = HIT_STORE;
								break;
		}
		EXPECT_EQ(l1_result.miss_hit, hitl1);
		EXPECT_EQ(l2_result.miss_hit, hitl2);
		EXPECT_EQ(status, OK);
	}
}

/*
 * TEST2: L1 miss L2 hit
 * 1. Choose a random entry info
 * 2. Fill a L1cache and L2cache entry
 * 3. Force a read/write miss
 * 4. Check miss in L1cache and hit in L2cache
 */
TEST_F(L2cache,l1_miss_l2_hit){
	int status = OK;
	bool debug = false;
	struct l1_l2_entry_info l1_l2_info;
	struct operation_result l1_result = {};
	struct operation_result l2_result = {};

	/* 1. Choose a random entry info */
	int associativityL1 = 1 << (rand()%4);
	int associativityL2 = associativityL1 << 1;

	if (debug_on) {
	printf("Entry Info\n associativity l1: %d\n associativity l2: %d\n",
				associativityL1,
				associativityL2);
	}
	/* associativity */
	l1_l2_info.l1_associativity = associativityL1;
	l1_l2_info.l2_associativity = associativityL2;
	/* Set entries (0 read 1 write)*/
	struct entry l1_cache_blocks[2][l1_l2_info.l1_associativity];
	struct entry l2_cache_blocks[2][l1_l2_info.l2_associativity];
	/* 2. Fill a L1cache and L2cache entry with known tag */
	for (int loadstore = 0 ; loadstore < 2; loadstore++){
		for (int wayl2 = 0; wayl2 < l1_l2_info.l2_associativity; wayl2++) {
			int rp_value = wayl2;
			int tag = wayl2;
			if (wayl2 < l1_l2_info.l1_associativity) {
				l1_cache_blocks[loadstore][wayl2].valid = true;
				l1_cache_blocks[loadstore][wayl2].tag = tag;
				l1_cache_blocks[loadstore][wayl2].dirty = 0;
				l1_cache_blocks[loadstore][wayl2].rp_value = rp_value;
			}
			l2_cache_blocks[loadstore][wayl2].valid = true;
			l2_cache_blocks[loadstore][wayl2].tag = tag;
			l2_cache_blocks[loadstore][wayl2].dirty = loadstore;
			l2_cache_blocks[loadstore][wayl2].rp_value = rp_value;
		}
	}
	/* 3. Force a read/write miss in L1 and hit in L2*/
	for (int loadstore = 0; loadstore < 2; loadstore++) {
		int tag = l1_l2_info.l2_associativity - 1;
		l1_l2_info.l1_tag = tag;
		l1_l2_info.l2_tag = tag;
		status = lru_replacement_policy_l1_l2(&l1_l2_info,
																					loadstore,
																					l1_cache_blocks[loadstore],
																					l2_cache_blocks[loadstore],
																					&l1_result,
																					&l2_result,
																					bool(debug_on));
		/* 4. Check miss in L1 and hit in L2cache */
		DEBUG(debug_on, l1_miss_l2_hit_test);
		int missl1;
		int hitl2;
		switch (loadstore) {
			case LOAD:
								missl1 = MISS_LOAD;
								hitl2 = HIT_LOAD;
								break;
			case STORE:
								missl1 = MISS_STORE;
								hitl2 = HIT_STORE;
								break;
		}
		EXPECT_EQ(l1_result.miss_hit, missl1);
		EXPECT_EQ(l2_result.miss_hit, hitl2);
		EXPECT_EQ(status, OK);
	}
}

/*
* TEST3: L1 miss L2 miss
* 1. Choose a random entry info
* 2. Fill a L1cache and L2cache entry
* 3. Force a read/write miss
* 4. Check miss in L1cache and miss in L2cache
* 5. Check dirty eviction
*/
TEST_F(L2cache,l1_miss_l2_miss){
	int status = OK;
	bool debug = false;
	struct l1_l2_entry_info l1_l2_info;
	struct operation_result l1_result = {};
	struct operation_result l2_result = {};

	/* 1. Choose a random entry info */
	int associativityL1 = 1 << (rand()%4);
	int associativityL2 = associativityL1 << 1;

	if (debug_on) {
	printf("Entry Info\n associativity l1: %d\n associativity l2: %d\n",
				associativityL1,
				associativityL2);
	}
	/* associativity */
	l1_l2_info.l1_associativity = associativityL1;
	l1_l2_info.l2_associativity = associativityL2;
	/* Set entries (0 read 1 write)*/
	struct entry l1_cache_blocks[2][l1_l2_info.l1_associativity];
	struct entry l2_cache_blocks[2][l1_l2_info.l2_associativity];
	/* 2. Fill a L1cache and L2cache entry with known tag */
	for (int loadstore = 0 ; loadstore < 2; loadstore++){
		for (int wayl2 = 0; wayl2 < l1_l2_info.l2_associativity; wayl2++) {
			int rp_value = wayl2;
			int tag = wayl2;
			if (wayl2 < l1_l2_info.l1_associativity) {
				l1_cache_blocks[loadstore][wayl2].valid = true;
				l1_cache_blocks[loadstore][wayl2].tag = tag;
				l1_cache_blocks[loadstore][wayl2].dirty = 0;
				l1_cache_blocks[loadstore][wayl2].rp_value = rp_value;
			}
			l2_cache_blocks[loadstore][wayl2].valid = true;
			l2_cache_blocks[loadstore][wayl2].tag = tag;
			l2_cache_blocks[loadstore][wayl2].dirty = loadstore;
			l2_cache_blocks[loadstore][wayl2].rp_value = rp_value;
		}
	}
	/* 3. Force a read/write miss in L1 and miss in L2*/
	for (int loadstore = 0; loadstore < 2; loadstore++) {
		int new_tag = 1024;
		l1_l2_info.l1_tag = new_tag;
		l1_l2_info.l2_tag = new_tag;
		status = lru_replacement_policy_l1_l2(&l1_l2_info,
																					loadstore,
																					l1_cache_blocks[loadstore],
																					l2_cache_blocks[loadstore],
																					&l1_result,
																					&l2_result,
																					bool(debug_on));
		/* 4. Check miss in L1 and miss in L2cache */
		DEBUG(debug_on, l1_miss_l2_miss_test);
		int missl1;
		int missl2;
		bool DIRTY;
		switch (loadstore) {
			case LOAD:
								missl1 = MISS_LOAD;
								missl2 = MISS_LOAD;
								DIRTY = false;
								break;
			case STORE:
								missl1 = MISS_STORE;
								missl2 = MISS_STORE;
								DIRTY = true;
								break;
		}
		EXPECT_EQ(l1_result.miss_hit, missl1);
		EXPECT_EQ(l2_result.miss_hit, missl2);
		/* 5. Check dirty eviction */
		EXPECT_EQ(l2_result.dirty_eviction, DIRTY);
		EXPECT_EQ(status, OK);
	}
}

/*
 * TEST4: Invalidation of L1 for victimization in L2
 * 1. Choose a random entry info
 * 2. Fill a L1cache entry
 * 3. Invalidate L1cache
 * 4. Check invalid block
 */
TEST_F(L2cache,l1_invalid_l2_victimization){
	int status = OK;
	bool debug = false;
	struct l1_l2_entry_info l1_l2_info;
	struct operation_result l1_result = {};

	/* 1. Choose a random entry info */
	int associativityL1 = 1 << (rand()%4);

	if (debug_on) {
	printf("Entry Info\n associativity l1: %d\n",
				associativityL1);
	}
	/* associativity */
	l1_l2_info.l1_associativity = associativityL1;
	/* Set entries (0 read 1 write)*/
	struct entry l1_cache_blocks[l1_l2_info.l1_associativity];
	/* 2. Fill a L1cache and L2cache entry with known tag */
	for (int wayl1 = 0; wayl1 < l1_l2_info.l1_associativity; wayl1++) {
		int rp_value = wayl1;
		int tag = wayl1;
		int invalid_way = wayl1;
		l1_cache_blocks[wayl1].valid = true;
		l1_cache_blocks[wayl1].tag = tag;
		l1_cache_blocks[wayl1].dirty = 0;
		l1_cache_blocks[wayl1].rp_value = rp_value;
	}
	int tag = l1_l2_info.l1_associativity -1;
	int invalid_way = tag;
	/* 3. Invalidate L1cache */
	status = l1_line_invalid_set(tag,
			                        l1_l2_info.l1_associativity,
			                        l1_cache_blocks,
			                        bool(debug_on));

	/* 4. Check invalid block */
	DEBUG(debug_on, l1_miss_l2_miss_test);
	EXPECT_EQ(status, OK);
	EXPECT_EQ(l1_cache_blocks[invalid_way].valid, INVALID);
}
