/*
 * 	Cache simulation project
 * 	Class UCR IE-521
 * 	Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>

using namespace std;

/* Get enviroment var */
void get_env_var( const char* var_name,
                 int *var_value )
{
    char* pPath = getenv(var_name);
    if (pPath==NULL) {
      *var_value = 0;
	} else {
	     *var_value= atoi(pPath);
	}
}

/* Print way info */
void print_way_info(int idx,
                    int l1_associativity,
                    entry* cache_blocks)
{
  int j = 0;
  for (j = 0 ; j< l1_associativity; j++) {
	printf(CYN "INFO: " RESET "Way #%d: tag: %d--- valid: %d rp_value: %d --- dirty: %d\n", j, cache_blocks[j].tag, cache_blocks[j].valid,cache_blocks[j].rp_value, cache_blocks[j].dirty);
  }

}

/*
 *  HELPER FUNCTIONS
 */

/* Print cache configuration */
void print_cache_config(uint16_t l1_cache_size_kb, uint16_t block_size_bytes, uint16_t l1_associativity, uint16_t optimization)
{
  string divisory_line = "\n____________________________________";

  cout << divisory_line;
  cout << "\nCache parameters:";
  cout << divisory_line;

  switch (optimization)
  {
    case VC:
      cout << "\nL1 Cache Size (KB): " << l1_cache_size_kb;
      cout << "\nCache L1 Associativity: " << l1_associativity;
      cout << "\nCache Block Size (bytes): " << block_size_bytes;
      break;
    case L2:
      cout << "\nL1 Cache Size (KB): " << l1_cache_size_kb;
      cout << "\nL2 Cache Size (KB): " << 4*l1_cache_size_kb;
      cout << "\nCache L1 Associativity: " << l1_associativity;
      cout << "\nCache L2 Associativity: " << 2*l1_associativity;
      cout << "\nCache Block Size (bytes): " << block_size_bytes;
      break;
  }
}

/* Get statistics */
void get_statistics( const operation_result* result , stat_result* stat_result)
{
  /* Get statistics */
    if (result->miss_hit == HIT_LOAD)
    {
      stat_result->load_hits++;
    } else if (result->miss_hit == MISS_LOAD)
    {
      stat_result->load_misses++;
    }else if (result->miss_hit == HIT_STORE)
    {
      stat_result->store_hits++;
    }else if (result->miss_hit == MISS_STORE)
    {
      stat_result->store_misses++;
    }

    if (result->dirty_eviction)
    {
      stat_result->dirty_evictions++;
    }
}

/* Print statistics */
void print_statistics(uint16_t optimization, const stat_result* l1_result, const stat_result* l2_result, const stat_result* vc_result)
{
  /* Victim cache */
  float l1_vc_hits = l1_result->load_hits + l1_result->store_hits + vc_result->load_hits + vc_result->store_hits;
  float l1_vc_misses = l1_result->load_misses + l1_result->store_misses + vc_result->load_misses + vc_result->store_misses;

  /* Multilevel cache */
  float l1_l2_hits = l1_result->load_hits + l1_result->store_hits + l2_result->load_hits + l2_result->store_hits;
  float l1_l2_misses = l1_result->load_misses + l1_result->store_misses + l2_result->load_misses + l2_result->store_misses;

  /* L1 cache */
  float l1_misses = l1_result->load_misses + l1_result->store_misses;
  float l1_hits = l1_result->load_hits + l1_result->store_hits;

  /* L2 cache */
  float l2_misses = l2_result->load_misses + l2_result->store_misses;
  float l2_hits = l2_result->load_hits + l2_result->store_hits;

  string divisory_line = "\n____________________________________";

  cout << divisory_line;
  cout << "\nSimulation results:";
  cout << divisory_line;

  switch (optimization)
  {
    case VC:
      cout << "\nMiss rate (L1+VC): " << l1_vc_misses / (l1_vc_hits + l1_vc_misses);
      cout << "\nMisses (L1+VC): " << (int)l1_vc_misses;
      cout << "\nHits (L1+VC): " << (int)l1_vc_hits;
      cout << "\nVictim cache hits: " << vc_result->load_hits + vc_result->store_hits;
      cout << "\nDirty evictions: " << l1_result->dirty_evictions;
      cout << divisory_line;
      cout << "\nFor debug:";
      cout << divisory_line;
      cout << "\nL1 misses : " << (int)l1_misses;
      cout << "\nL1 hits : " << (int)l1_hits;
      cout << "\nL1 de : " << l1_result->dirty_evictions ;
      cout << "\nVC misses : " << vc_result->load_misses + vc_result->store_misses;
      cout << "\nVC hits : " << vc_result->load_hits + vc_result->store_hits << "\n";
      break;
    case L2:
      //cout << "\nOverall miss rate: " << l1_l2_misses / (l1_l2_hits + l1_l2_misses);
      cout << "\nL1 miss rate: " << l1_misses/ (l1_misses + l1_hits);
      cout << "\nL2 miss rate: " << l2_misses / (l2_misses + l2_hits);
      cout << "\nGlobal miss rate: " << l2_misses / (l1_misses + l1_hits);
      cout << "\nMisses (L1): " << l1_misses;
      cout << "\nHits (L1): " << l1_hits;
      cout << "\nMisses (L2): " << l2_misses;
      cout << "\nHits (L2): " << l2_hits;
      cout << "\nDirty evictions (L2): " << l2_result->dirty_evictions;
      cout << divisory_line << "\n";
      break;
  }
}
